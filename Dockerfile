FROM node:14-alpine
WORKDIR /app
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
COPY index.js /app/index.js
COPY views/ /app/views/
RUN npm install

EXPOSE 3000

ENTRYPOINT ["node", "index.js"]